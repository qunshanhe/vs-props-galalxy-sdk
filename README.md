# VsProps-GalalxySDK

#### 介绍
Visual Studio 属性表 - 大恒Galaxy系列相机SDK


#### 使用说明
下载本文件，打包在GalaxySDK文件夹中，拷贝到在需要使用大恒相机SDK的工程的Visual Studio项目文件夹里。 
随后，在属性表的对应平台添加属性表props文件。
